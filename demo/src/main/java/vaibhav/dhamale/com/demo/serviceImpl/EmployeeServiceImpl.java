package vaibhav.dhamale.com.demo.serviceImpl;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

import vaibhav.dhamale.com.demo.bo.Employee;
import vaibhav.dhamale.com.demo.constants.commonConstants;
import vaibhav.dhamale.com.demo.service.EmployeeService;
@Service
public class EmployeeServiceImpl implements EmployeeService {

	private static Map<String,Employee> allEmployees= new HashMap<>();
	private static Map<String,String> allStatus= new HashMap<>();
	@Override
	public  Map<String,Employee>  addEmployees(List<Employee> employees) {
		for (Employee employee : employees) {
			allEmployees.put(employee.getEmployeeId()+"",employee);
		}
		return allEmployees;
	}

	@Override
	public  Map<String,Employee> updateEmployees(List<Employee> employees) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public  Map<String,String>  deleteEmployees(List<String> employees) {
		for (String employeeid : employees) {
			if(allEmployees.containsKey(employeeid))
			{
				Employee emp=allEmployees.get(employeeid);
				allStatus.put(emp.getEmployeeId()+"",commonConstants.EmployeeAdded);
				allEmployees.remove(employeeid);
				
			}
		}
		return allStatus;
	}

	@Override
	public Map<String,Employee> getEmployee(String employeeid) {
		if(allEmployees.containsKey(employeeid))
		{
			Employee emp=allEmployees.get(employeeid);
			Map<String,Employee> searchEmployee= new HashMap<>();
			searchEmployee.put(employeeid, emp);
			return searchEmployee;
		}
		return null;
	}

	@Override
	public Map<String, Employee> getEmployees() {
		return allEmployees;
	}

	

}
