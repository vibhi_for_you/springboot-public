package vaibhav.dhamale.com.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vaibhav.dhamale.com.demo.bo.Employee;
import vaibhav.dhamale.com.demo.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employee;

	// Add Employee
	@RequestMapping(value = "/Employee", method = RequestMethod.POST)
	public Map<String,Employee> addEmployee(@RequestBody List<Employee> employees) {
		return employee.addEmployees(employees);
	}
	
	//Get All Employee
	@RequestMapping(value = "/Employee", method = RequestMethod.GET)
	public Map<String,Employee> getEmployee() {
		return employee.getEmployees();
	}
	
	//Get id Employee
	@RequestMapping(value = "/Employee/{id}", method = RequestMethod.GET)
	public Map<String,Employee> getEmployee(@PathVariable String id) {
		return employee.getEmployee(id);
	}
	
	// Delete Employee
	@RequestMapping(value = "/Employee", method = RequestMethod.DELETE)
	public Map<String,String> deleteEmployee(@RequestBody List<String> employees) {
		return employee.deleteEmployees(employees);
	}

}