package vaibhav.dhamale.com.demo.bo;

public class Employee {

	private int employeeId;
	private String employeeName;
	private String employeeAdderss;
	private String status;
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeAdderss() {
		return employeeAdderss;
	}
	public void setEmployeeAdderss(String employeeAdderss) {
		this.employeeAdderss = employeeAdderss;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
