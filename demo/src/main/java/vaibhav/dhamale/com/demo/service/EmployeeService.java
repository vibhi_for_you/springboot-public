package vaibhav.dhamale.com.demo.service;

import java.util.List;
import java.util.Map;

import vaibhav.dhamale.com.demo.bo.Employee;

public interface EmployeeService {

	public Map<String, Employee> addEmployees(List<Employee> employees);

	public Map<String, Employee> updateEmployees(List<Employee> employees);

	public Map<String, String> deleteEmployees(List<String> employees);

	public Map<String, Employee> getEmployee(String employeeid);

	public Map<String, Employee> getEmployees();

}
